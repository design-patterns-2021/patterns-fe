import {CreateFormObserver} from './create-form.observer';
import {NoteTypeEnum} from '../api/enums/note-type.enum';

export abstract class Subject {
  private _observers: Array<CreateFormObserver>;

  constructor() {
    this._observers = new Array<CreateFormObserver>();
  }

  public registerObserver(observer: CreateFormObserver): void {
    this._observers.push(observer);
  }

  public notify(noteType: NoteTypeEnum): void {
    for (let observer of this._observers) {
      observer.show(noteType);
    }
  }
}
