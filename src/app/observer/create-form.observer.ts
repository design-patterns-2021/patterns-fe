import {NoteTypeEnum} from '../api/enums/note-type.enum';

export interface CreateFormObserver {
  show(noteType: NoteTypeEnum): void;
}
