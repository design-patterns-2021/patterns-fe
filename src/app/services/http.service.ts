import {Observable} from 'rxjs';

export interface HttpService<T> {
  doGet?(...parameters: any[]): Observable<T>;

  doPost?(...parameters: any[]): Observable<T>;
}
