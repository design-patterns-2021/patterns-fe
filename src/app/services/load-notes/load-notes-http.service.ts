import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NoteResponse} from '../../api/domain/note-response';
import {environment} from '../../../environments/environment';
import {HttpService} from '../http.service';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class LoadNotesHttpService implements HttpService<NoteResponse[]> {

  constructor(private httpClient: HttpClient) {
  }

  public doGet(): Observable<NoteResponse[]> {
    return this.httpClient.get<NoteResponse[]>(this._getUrl());
  }

  private _getUrl(): string {
    return `${environment.host}/notes`;
  }
}
