import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {NoteResponse} from '../../api/domain/note-response';
import {HttpService} from '../http.service';
import {LoadNotesHttpService} from './load-notes-http.service';
import {environment} from '../../../environments/environment';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class LoadNotesHttpProxyService implements HttpService<NoteResponse[]> {

  constructor(private loadNotesHttpService: LoadNotesHttpService) {
  }

  public doGet(): Observable<NoteResponse[]> {
    this._logging();
    return this.loadNotesHttpService.doGet();
  }

  private _logging(): void {
    if (environment.logging) {
      console.table({Action: 'GET', user: 'Alain Quinones', publishedTo: 'ELK'});
    }
  }
}
