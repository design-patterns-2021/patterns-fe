import {NoteTypeEnum} from '../enums/note-type.enum';

export class NoteResponse {
  private id: string;
  public title: string;
  public date: Date;
  public noteType: NoteTypeEnum;
  public username: string;
  public lastName: string;

  private readonly _EMPTY = '';

  constructor() {
    this.id = this._EMPTY;
    this.title = this._EMPTY;
    this.date = new Date();
    this.noteType = NoteTypeEnum.TODO;
    this.username = this._EMPTY;
    this.lastName = this._EMPTY;
  }
}
