import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DropdownModule} from 'primeng/dropdown';
import {FormsModule} from '@angular/forms';
import {TodoComponent} from './components/todo/todo.component';
import {EventComponent} from './components/event/event.component';
import {ButtonModule} from 'primeng/button';
import {HttpClientModule} from '@angular/common/http';
import {NotesComponent} from './components/notes/notes.component';
import {NoteComponent} from './components/note/note.component';
import {CreateNoteComponent} from './components/create-note/create-note.component';
import {HeaderComponent} from './components/header/header.component';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    EventComponent,
    NotesComponent,
    NoteComponent,
    CreateNoteComponent,
    HeaderComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    DropdownModule,
    ButtonModule,
    HttpClientModule,
    InputTextModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
