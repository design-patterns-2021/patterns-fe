import {Component, OnInit} from '@angular/core';
import {NoteTypeEnum} from './api/enums/note-type.enum';
import {NoteResponse} from './api/domain/note-response';
import {CreateFormObserver} from './observer/create-form.observer';
import {LoadNotesHttpProxyService} from './services/load-notes/load-notes-http.proxy.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, CreateFormObserver {

  public notes: NoteResponse[];
  public noteTypeToCreate: NoteTypeEnum;
  public showNoteForm: boolean;

  constructor(private _loadNotesHttpService: LoadNotesHttpProxyService) {
    this.notes = [];
    this.showNoteForm = false;
  }

  ngOnInit(): void {
    this._initialize();
  }

  show(noteType: NoteTypeEnum): void {
    if (noteType !== null) {
      this.noteTypeToCreate = noteType;
      this.showNoteForm = true;
    } else {
      this.noteTypeToCreate = null;
      this.showNoteForm = false;
    }
  }

  private _initialize(): void {
    this._loadNotesHttpService.doGet().subscribe((notes: NoteResponse[]) => {
      this.notes = notes;
    });
  }
}
