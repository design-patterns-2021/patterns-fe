import {Component} from '@angular/core';
import {NoteOperation} from '../note/render/note-operation';
import {NoteResponse} from '../../api/domain/note-response';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent {
  public title: string;
  public date: Date;

  public loadInformation(noteOperation: NoteOperation, note: NoteResponse): void {
    noteOperation.loadEventComponent(this, note);
  }
}
