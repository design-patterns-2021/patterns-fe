import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {NoteTypeEnum} from '../../api/enums/note-type.enum';
import {TodoComponent} from '../todo/todo.component';
import {EventComponent} from '../event/event.component';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss']
})
export class CreateNoteComponent implements AfterViewInit {
  @Input() public noteType: NoteTypeEnum;

  @ViewChild('container', {read: ViewContainerRef, static: true}) private _container: ViewContainerRef;

  constructor(private _componentFactoryResolver: ComponentFactoryResolver,
              private _cdr: ChangeDetectorRef) {
    this._cdr.detach();
  }

  ngAfterViewInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    const component: Type<TodoComponent | EventComponent> = this.noteType === NoteTypeEnum.TODO ? TodoComponent : EventComponent;

    const factory: ComponentFactory<any> = this._componentFactoryResolver.resolveComponentFactory(component);
    const componentRef: ComponentRef<any> = this._container.createComponent(factory);
    const instance: TodoComponent | EventComponent = componentRef.instance;

    this._cdr.detectChanges();
    this._cdr.reattach();
  }
}
