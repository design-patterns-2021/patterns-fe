import {Component} from '@angular/core';
import {NoteOperation} from '../note/render/note-operation';
import {NoteResponse} from '../../api/domain/note-response';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {

  public title: string;
  public date: Date;
  public username: string;
  public lastName: string;

  public loadInformation(noteOperation: NoteOperation, note: NoteResponse): void {
    noteOperation.loadTodoComponent(this, note);
  }
}
