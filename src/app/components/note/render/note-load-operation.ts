import {NoteOperation} from './note-operation';
import {EventComponent} from '../../event/event.component';
import {TodoComponent} from '../../todo/todo.component';
import {NoteResponse} from '../../../api/domain/note-response';

export class NoteLoadOperation implements NoteOperation {

  public loadTodoComponent(component: TodoComponent, note: NoteResponse): void {
    component.title = note.title;
    component.date = note.date;
    component.username = note.username;
    component.lastName = note.lastName;
  }

  public loadEventComponent(component: EventComponent, note: NoteResponse): void {
    component.date = note.date;
    component.title = note.title;
  }
}
