import {TodoComponent} from '../../todo/todo.component';
import {EventComponent} from '../../event/event.component';
import {NoteResponse} from '../../../api/domain/note-response';

export interface NoteOperation {
  loadTodoComponent(component: TodoComponent, note: NoteResponse): void;

  loadEventComponent(component: EventComponent, note: NoteResponse): void;
}
