import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {NoteResponse} from '../../api/domain/note-response';
import {TodoComponent} from '../todo/todo.component';
import {EventComponent} from '../event/event.component';
import {NoteTypeEnum} from '../../api/enums/note-type.enum';
import {NoteLoadOperation} from './render/note-load-operation';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements AfterViewInit {
  @Input() public note: NoteResponse;

  @ViewChild('container', {read: ViewContainerRef, static: true}) private _container: ViewContainerRef;

  constructor(private _componentFactoryResolver: ComponentFactoryResolver,
              private _cdr: ChangeDetectorRef) {
    this._cdr.detach();
  }

  ngAfterViewInit(): void {
    this._initialize();
  }


  private _initialize(): void {
    const component: Type<TodoComponent | EventComponent> = this.note.noteType === NoteTypeEnum.TODO ? TodoComponent : EventComponent;

    const factory: ComponentFactory<any> = this._componentFactoryResolver.resolveComponentFactory(component);
    const componentRef: ComponentRef<any> = this._container.createComponent(factory);
    const instance: TodoComponent | EventComponent = componentRef.instance;

    instance.loadInformation(new NoteLoadOperation(), this.note);

    this._cdr.detectChanges();
    this._cdr.reattach();
  }
}
