import {Component, Input} from '@angular/core';
import {NoteResponse} from '../../api/domain/note-response';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent {
  @Input() public notes: NoteResponse[];

  constructor() {
    this.notes = [];
  }
}
