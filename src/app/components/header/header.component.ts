import {AfterViewInit, Component, Input} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {NoteTypeEnum} from '../../api/enums/note-type.enum';
import {CreateFormObserver} from '../../observer/create-form.observer';
import {Subject as SubjectAngular} from 'rxjs';
import {Subject} from '../../observer/subject';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends Subject implements AfterViewInit {

  public static onInputSearch: SubjectAngular<string> = new SubjectAngular<string>();

  @Input() private appComponent: CreateFormObserver;

  private _searchWord: string;

  public readonly options: SelectItem[] = [
    {label: 'What do you want to create?', value: null},
    {label: 'Event', value: NoteTypeEnum.EVENT},
    {label: 'To Do', value: NoteTypeEnum.TODO}
  ];

  constructor() {
    super();
  }

  ngAfterViewInit(): void {
    this.registerObserver(this.appComponent);
  }

  public onOptionSelected(event: { originalEvent: PointerEvent, value: string }): void {
    event.originalEvent.preventDefault();
    this.notify(event.value as NoteTypeEnum);
  }


  get searchWord(): string {
    return this._searchWord;
  }

  set searchWord(value: string) {
    this._searchWord = value;
    HeaderComponent.onInputSearch.next(this._searchWord);
  }
}
