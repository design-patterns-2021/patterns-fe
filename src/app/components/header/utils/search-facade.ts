import {NoteResponse} from '../../../api/domain/note-response';

export class SearchFacade {

  public User: string;

  public search(notes: NoteResponse[], userInput: string): NoteResponse[] {
    return notes.filter((note: NoteResponse) => note.title.includes(userInput));
  }
}
