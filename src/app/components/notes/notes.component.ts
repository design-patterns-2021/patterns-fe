import {AfterViewInit, Component, Input} from '@angular/core';
import {NoteResponse} from '../../api/domain/note-response';
import {SearchFacade} from '../header/utils/search-facade';
import {HeaderComponent} from '../header/header.component';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements AfterViewInit {
  @Input() public notes: NoteResponse[];
  private notesCopy: NoteResponse[];

  private _searchFacade: SearchFacade;

  constructor() {
    this.notes = [];
    this.notesCopy = [];
    this._searchFacade = new SearchFacade();
  }

  ngAfterViewInit(): void {
    this.notesCopy = [...this.notes];
    HeaderComponent.onInputSearch.asObservable().subscribe((inputSearch: string) => {
      this.notes = (inputSearch !== '') ? this._searchFacade.search(this.notes, inputSearch) : this.notesCopy;
    });
  }
}
